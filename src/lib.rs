#![feature(core_intrinsics)]
#![no_std]


extern crate libc;
extern crate approx_eq;
extern crate num;
extern crate round;
extern crate trig;
extern crate sqrt;


mod float;


pub use float::Float;
